package org.feup.bondpoint;

import java.util.Collection;

import android.app.Application;

import com.facebook.model.GraphUser;

public class GlobalData extends Application {

	private int nFriends = -1;
	private Friend[] friends = null;
	private BondPoint[] loadedBondPoints = null;
	private BondPoint[] newBondPoints = null;

	private Collection<GraphUser> invitedFriends;

	public GlobalData() {
		super();
	}

}
