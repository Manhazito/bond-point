package org.feup.bondpoint;

import android.graphics.Bitmap;

import com.google.android.gms.maps.model.LatLng;

public class Friend {
	private String facebookId = "";
	private String name = "";
	private LatLng position = null;
	private Bitmap image = null;
	private boolean hasBondPointApp = false;

	public Friend() {

	}

	public Friend(String fFacebookId, String fName, LatLng fPosition,
			Bitmap fImage, boolean fHasBondPointApp) {
		this.facebookId = fFacebookId;
		this.name = fName;
		this.position = fPosition;
		this.image = fImage;
		this.hasBondPointApp = fHasBondPointApp;

	}

	public Friend(String fFacebookId, String fName, String latitude,
			String longitude, Bitmap fImage, boolean fHasBondPointApp) {
		this.facebookId = fFacebookId;
		this.name = fName;
		this.position = new LatLng(Double.parseDouble(latitude),
				Double.parseDouble(longitude));
		this.image = fImage;
		this.hasBondPointApp = fHasBondPointApp;

	}

	public String getFacebookId() {
		return facebookId;
	}

	public void setFacebookId(String facebookId) {
		this.facebookId = facebookId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public LatLng getPosition() {
		return position;
	}

	public void setPosition(LatLng position) {
		this.position = position;
	}

	public Bitmap getImage() {
		return image;
	}

	public void setImage(Bitmap image) {
		this.image = image;
	}

	public boolean isHasBondPointApp() {
		return hasBondPointApp;
	}

	public void setHasBondPointApp(boolean hasBondPointApp) {
		this.hasBondPointApp = hasBondPointApp;
	}

}
